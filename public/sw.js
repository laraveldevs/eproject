importScripts(
    "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

if (workbox)
{
    console.log(`Workbox is loaded`);

    self.__precacheManifest = [].concat(self.__precacheManifest || []);
    workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

    const filesToCache = [
        "/",
        "/index.html",
        "/public/img/icons/favicon-32x32.png",
        "/public/favicon.ico",
        "/public/img/icons/android-chrome-192x192.png",
        "/public/img/icons/favicon-16x16.png",
    ];

    self.addEventListener("install", function (e)
    {
        /*e.waitUntil(
            caches.open("test-cache").then(function (cache)
            {
                return cache.addAll(filesToCache);
            })
        );*/
    });

    self.addEventListener("fetch", function (event)
    {
        /*console.log(event.request.url);

        event.respondWith(
            caches.match(event.request).then(function (response)
            {
                return response || fetch(event.request);
            })
        );*/
    });
} else
{
    console.log(`Workbox didn't load`);
}
