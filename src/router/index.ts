import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import ProjectsView from '../views/ProjectsView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'ProjectsView',
    component: ProjectsView
  },
  {
    path: '/activities/:projectId',
    name: 'ActivityView',
    component: () => import(/* webpackChunkName: "activityview" */ '../views/ActivityView.vue')
  },
  {
    path: '/tasks/:activityId',
    name: 'TaskView',
    component: () => import(/* webpackChunkName: "taskview" */ '../views/TaskView.vue')
  },
  {
    path: '/project/details/:project',
    name: 'ProjectIndex',
    component: () => import(/* webpackChunkName: "projectindex" */ '../views/ProjectIndex.vue')
  },
  {
    path: '/activity/details/:activity',
    name: 'ActivityIndex',
    component: () => import(/* webpackChunkName: "activityindex" */ '../views/ActivityIndex.vue')
  },
  {
    path: '/task/details/:task',
    name: 'TaskIndex',
    component: () => import(/* webpackChunkName: "taskindex" */ '../views/TaskIndex.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
