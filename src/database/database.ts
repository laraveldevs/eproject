import { Priority } from "@/models/Priority";
import { State } from "@/models/State";

export default {
    projects: [
        {
            "ID": "p1",
            "Name": "MANUTENZIONE STRAORDINARIA",
            "Cost": 150,
            "StartDate": "01/03/2021",
            "Progress": 0,
            "EndDate": "30/06/2021",
            "ExpectedTime": 100,
            "WorkedTime": 0,
            "State": State.Stopped,
            "Priority": Priority.High,
            "SellPrice": 80,
            "Technicians": [
                "1",
                "2",
                "3"
            ],
            "Activities": [
                "a1",
                "a2"
            ]
        }
    ],
    "activities": [
        {
            "ID": "a1",
            "Name": "ATTIVITA' 1",
            "Cost": 150,
            "StartDate": "01/03/2021",
            "Progress": 0,
            "EndDate": "15/04/2021",
            "ExpectedTime": 100,
            "WorkedTime": 0,
            "State": State.Stopped,
            "Priority": Priority.Medium,
            "SellPrice": 80,
            "Project": "p1",
            "Technicians": [
                "1"
            ],
            "Tasks": [
                "t1"
            ]
        },
        {
            "ID": "a2",
            "Name": "ATTIVITA' 2",
            "Cost": 150,
            "StartDate": "01/03/2021",
            "Progress": 0,
            "EndDate": "30/06/2021",
            "ExpectedTime": 100,
            "WorkedTime": 0,
            "State": State.Stopped,
            "Priority": Priority.High,
            "SellPrice": 80,
            "Project": "p1",
            "Technicians": [
                "2",
                "3"
            ],
            "Tasks": [
                "t2",
                "t3"
            ]
        }
    ],
    "tasks": [
        {
            "ID": "t1",
            "Name": "TASK 1",
            "Cost": 150,
            "StartDate": "01/03/2021",
            "Progress": 0,
            "EndDate": "15/04/2021",
            "ExpectedTime": 100,
            "WorkedTime": 0,
            "State": State.Stopped,
            "Priority": Priority.Medium,
            "SellPrice": 80,
            "Project": "p1",
            "Activity": "a1",
            "Technicians": [
                "1"
            ]
        },
        {
            "ID": "t2",
            "Name": "TASK 2",
            "Cost": 150,
            "StartDate": "01/03/2021",
            "Progress": 0,
            "EndDate": "20/05/2021",
            "ExpectedTime": 100,
            "WorkedTime": 0,
            "State": State.Stopped,
            "Priority": Priority.Low,
            "SellPrice": 80,
            "Project": "p1",
            "Activity": "a2",
            "Technicians": [
                "2"
            ]
        },
        {
            "ID": "t3",
            "Name": "TASK 3",
            "Cost": 150,
            "StartDate": "13/04/2021",
            "Progress": 0,
            "EndDate": "30/06/2021",
            "ExpectedTime": 100,
            "WorkedTime": 0,
            "State": State.Stopped,
            "Priority": Priority.Medium,
            "SellPrice": 80,
            "Project": "p1",
            "Activity": "a2",
            "Technicians": [
                "3"
            ]
        }
    ],
    "technician": [
        {
            "ID": "1",
            "Name": "Mario",
            "Surname": "Rossi",
            "Available": true,
            "Project": "p1",
            "Activity": "a1",
            "Task": "t1"
        },
        {
            "ID": "2",
            "Name": "Giorgio",
            "Surname": "Verdi",
            "Available": true,
            "Project": "p1",
            "Activity": "a2",
            "Task": "t2"
        },
        {
            "ID": "3",
            "Name": "Paolo",
            "Surname": "Bianchi",
            "Available": true,
            "Project": "p1",
            "Activity": "a2",
            "Task": "t3"
        }
    ]
}