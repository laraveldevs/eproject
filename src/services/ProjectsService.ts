import Filter from "@/models/Filter";
import Project from "@/models/Project";
import { Result } from "@/models/Result";
import { Status } from "@/models/Status";
import { IService } from "./IService"

import database from '../database/database';

export default class ProjectsService implements IService<Project>
{
    post = async <V>(data: Project, ...params: Array<V>): Promise<Result<Status, Project>> =>
    {
        return new Result(Status.Completed);
    }

    get = async (params?: Array<string>): Promise<Result<Status, Array<Project>>> =>
    {
        const res: Array<Project> = (params) ? database.projects.filter((p: Project) =>
        {
            return params?.includes(p.ID);
        }) : database.projects;

        return new Result(Status.Completed, res);
    }

    filter = async (fields: Filter): Promise<Result<Status, Array<Project>>> =>
    {
        const checkName: boolean = (fields.Name !== undefined) && (fields.Name !== "");
        const checkStart: boolean = (fields.StartDate !== undefined) && (fields.StartDate !== null) && (fields.StartDate.toString() !== "");
        const checkEnd: boolean = (fields.EndDate !== undefined) && (fields.EndDate !== null) && (fields.EndDate.toString() !== "");
        const checkWorked: boolean = (fields.WorkedTime !== undefined) && (fields.WorkedTime !== 0) && (fields.WorkedTime.toString() !== "");
        const checkExpected: boolean = (fields.ExpectedTime !== undefined) && (fields.ExpectedTime !== 0) && (fields.ExpectedTime.toString() !== "");

        const checkError = [checkName, checkStart, checkEnd, checkWorked, checkExpected];

        if (checkError.every((c) => c === false))
        {
            return new Result(Status.Completed, database.projects);
        }

        const nameIntersect = database.projects?.filter((p: Project) =>
        {
            return checkName ? (fields.Name === p.Name) : true;
        });

        const startIntersect = nameIntersect?.filter((p: Project) =>
        {
            return checkStart ? (fields.StartDate === p.StartDate) : true;
        });

        const endIntersect = startIntersect?.filter((p: Project) =>
        {
            return checkEnd ? (fields.EndDate === p.EndDate) : true;
        });

        const workedIntersect = endIntersect?.filter((p: Project) =>
        {
            return checkWorked ? (fields.WorkedTime === p.WorkedTime) : true;
        });

        const expectedIntersect = workedIntersect?.filter((p: Project) =>
        {
            return checkExpected ? (fields.ExpectedTime === p.ExpectedTime) : true;
        });

        return new Result(Status.Completed, expectedIntersect);
    }
}