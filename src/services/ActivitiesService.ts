import database from "@/database/database";
import Activity from "@/models/Activity";
import Filter from "@/models/Filter";
import { Result } from "@/models/Result";
import { State } from "@/models/State";
import { Status } from "@/models/Status";
import { IService } from "./IService";

export default class ActivitiesService implements IService<Activity>
{
    post = async <V>(data: Activity, ...params: Array<V>): Promise<Result<Status, Activity>> =>
    {
        return new Result(Status.Completed, data);
    }

    get = async (params?: Array<string>): Promise<Result<Status, Array<Activity>>> =>
    {
        const res: Array<Activity> = (params) ? database.activities.filter((a: Activity) =>
        {
            return params?.includes(a.ID);
        }) : database.activities;

        return new Result(Status.Completed, res);
    }

    filter = async (fields: Filter): Promise<Result<Status, Array<Activity>>> =>
    {
        const checkName: boolean = (fields.Name !== undefined) && (fields.Name !== "");
        const checkStart: boolean = (fields.StartDate !== undefined) && (fields.StartDate !== null) && (fields.StartDate.toString() !== "");
        const checkEnd: boolean = (fields.EndDate !== undefined) && (fields.EndDate !== null) && (fields.EndDate.toString() !== "");
        const checkWorked: boolean = (fields.WorkedTime !== undefined) && (fields.WorkedTime !== 0) && (fields.WorkedTime.toString() !== "");
        const checkExpected: boolean = (fields.ExpectedTime !== undefined) && (fields.ExpectedTime !== 0) && (fields.ExpectedTime.toString() !== "");

        const checkError = [checkName, checkStart, checkEnd, checkWorked, checkExpected];

        if (checkError.every((c) => c === false))
        {
            return new Result(Status.Completed);
        }

        const nameIntersect = database.activities?.filter((p: Activity) =>
        {
            return checkName ? (fields.Name === p.Name) : true;
        });

        const startIntersect = nameIntersect.filter((p: Activity) =>
        {
            return checkStart ? (fields.StartDate === p.StartDate) : true;
        });

        const endIntersect = startIntersect.filter((p: Activity) =>
        {
            return checkEnd ? (fields.EndDate === p.EndDate) : true;
        });

        const workedIntersect = endIntersect.filter((p: Activity) =>
        {
            return checkWorked ? (fields.WorkedTime === p.WorkedTime) : true;
        });

        const expectedIntersect = workedIntersect.filter((p: Activity) =>
        {
            return checkExpected ? (fields.ExpectedTime === p.ExpectedTime) : true;
        });

        return new Result(Status.Completed, expectedIntersect);
    }
}