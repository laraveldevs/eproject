import { Result } from "@/models/Result";
import { Status } from "@/models/Status";

export interface IService<T>
{
    post: <V>(data: T, ...params: Array<V>) => Promise<Result<Status, T>>;

    get: (params?: Array<string>) => Promise<Result<Status, Array<T>>>;
}