import database from "@/database/database";
import { Result } from "@/models/Result";
import { Status } from "@/models/Status";
import Technician from "@/models/Technician";
import { IService } from "./IService";

export default class TechnicianService implements IService<Technician>
{
    post = async <V>(data: Technician, ...params: Array<V>): Promise<Result<Status, Technician>> =>
    {
        return new Result(Status.Completed);
    }

    get = async (params?: Array<string>): Promise<Result<Status, Array<Technician>>> =>
    {
        const res: Array<Technician> = (params) ? database.technician.filter((t: Technician) =>
        {
            return params?.includes(t.ID);
        }) : database.technician;

        return new Result(Status.Completed, res);
    }
}