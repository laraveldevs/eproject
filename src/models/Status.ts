export enum Status
{
    Completed,
    Pending,
    Error,
    Undefined
}