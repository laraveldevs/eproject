export enum State
{
    Started = "Partito",
    Completed = "Completato",
    Stopped = "Fermato",
    Deleted = "Eliminato"
}