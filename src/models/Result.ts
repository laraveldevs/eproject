export class Result<S, V>
{
    status: S;
    value?: V;
    message?: string;

    public constructor(status: S, value?: V, message?: string)
    {
        this.status = status;
        this.value = value;
        this.message = message;
    }
}