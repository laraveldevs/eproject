export enum Priority
{
    High = "Alta",
    Medium = "Media",
    Low = "Bassa"
}