export default interface Filter
{
    Name?: string,
    StartDate?: String,
    EndDate?: String,
    ExpectedTime?: number,
    WorkedTime?: number,
}