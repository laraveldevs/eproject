import Activity from "./Activity";
import Project from "./Project"
import Task from "./Task";
import Technician from "./Technician";

export default interface Database
{
    projects?: Array<Project>;
    activities?: Array<Activity>;
    tasks?: Array<Task>;
    technicians?: Array<Technician>;
}