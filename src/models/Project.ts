import { Priority } from "./Priority";
import { State } from "./State";

export default interface Project
{
    ID: string,
    Name: string,
    StartDate?: string,
    EndDate?: string,
    Technicians?: Array<string>,
    Cost?: number,
    SellPrice?: number,
    Progress?: number,
    Activities?: Array<string>,
    State?: State,
    Priority?: Priority,
    ExpectedTime?: number,
    WorkedTime?: number
}