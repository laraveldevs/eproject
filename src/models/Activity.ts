import { Priority } from "./Priority";
import { State } from "./State";

export default interface Activity
{
    ID: string,
    Project?: string,
    Name: string,
    StartDate?: string,
    EndDate?: string,
    Technicians?: Array<string>,
    Tasks?: Array<string>,
    SellPrice?: number,
    Cost?: number,
    Progress?: number,
    ExpectedTime?: number,
    WorkedTime?: number,
    State?: State,
    Priority?: Priority
}