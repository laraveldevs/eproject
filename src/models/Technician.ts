export default interface Technician
{
    ID: string,
    Name: string,
    Surname: string,
    Available: boolean,
    Project?: string,
    Activity?: string,
    Task?: string
}